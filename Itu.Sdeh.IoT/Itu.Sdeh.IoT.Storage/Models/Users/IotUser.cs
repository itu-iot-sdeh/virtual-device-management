﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Itu.Sdeh.IoT.Storage.Models.Events;

namespace Itu.Sdeh.IoT.Storage.Models.Users
{
    public class IotUser
    {
        [Key]
        public int Id { get; set; }
        //TODO: Implement user

        [InverseProperty(nameof(DeviceConfigured.User))]
        public virtual ICollection<DeviceConfigured> DevicesConfigured { get; set; }
    }
}
