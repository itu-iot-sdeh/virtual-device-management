﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Itu.Sdeh.IoT.Storage.Models.Entities.Credentials
{
    public class WatsonDeviceCredential
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string OrganizationId { get; set; }

        [Required]
        public string AuthenticationToken { get; set; }

        [Required]
        [ForeignKey("VirtualEntity")]
        public string VirtualEntityFk { get; set; }
        public virtual VirtualEntity VirtualEntity { get; set; }
    }
}
