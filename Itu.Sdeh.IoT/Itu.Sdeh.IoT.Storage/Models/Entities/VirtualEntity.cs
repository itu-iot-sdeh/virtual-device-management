﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Itu.Sdeh.IoT.Storage.Models.Entities.Credentials;
using Itu.Sdeh.IoT.Storage.Models.Events;

namespace Itu.Sdeh.IoT.Storage.Models.Entities
{
    public class VirtualEntity
    {
        [Key]
        public string VirtualId { get; set; }

        [Required]
        public string DeviceType { get; set; }

        [Required]
        public bool IsStandby { get; set; }

        [InverseProperty(nameof(WatsonDeviceCredential.VirtualEntity))]
        public virtual WatsonDeviceCredential WatsonCredential { get; set; }

        [InverseProperty(nameof(Entities.PhysicalEntity.VirtualEntity))]
        public virtual PhysicalEntity PhysicalEntity { get; set; }

        [InverseProperty(nameof(DeviceConfigured.SourceVirtualEntity))]
        public virtual ICollection<DeviceConfigured> SourceConfigurationEvents { get; set; }

        [InverseProperty(nameof(DeviceConfigured.DestinationVirtualEntity))]
        public virtual ICollection<DeviceConfigured> DestinationConfigurationEvents { get; set; }
    }
}
