﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Itu.Sdeh.IoT.Storage.Models.Events;

namespace Itu.Sdeh.IoT.Storage.Models.Entities
{
    public class PhysicalEntity
    {
        [Key]
        public string HardwareId { get; set; }

        [Required]
        [ForeignKey("VirtualEntity")]
        public string VirtualEntityFk { get; set; }
        public virtual VirtualEntity VirtualEntity { get; set; }

        [InverseProperty(nameof(DeviceConfigured.PhysicalEntity))]
        public virtual ICollection<DeviceConfigured> ConfigurationEvents { get; set; }
    }
}
