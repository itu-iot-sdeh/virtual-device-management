﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Itu.Sdeh.IoT.Storage.Models.Entities;
using Itu.Sdeh.IoT.Storage.Models.Users;

namespace Itu.Sdeh.IoT.Storage.Models.Events
{
    public class DeviceConfigured
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public DateTime TimeOfEvent { get; set; }

        /// <summary>
        /// Identifies the uniqueness of an event. A single event means a unique guid - a swap means two guids
        /// </summary>
        [Required]
        public string EventGuid { get; set; }

        [Required]
        [ForeignKey("PhysicalEntity")]
        public string PhysicalEntityFk { get; set; }
        public virtual PhysicalEntity PhysicalEntity { get; set; }

        [ForeignKey("SourceVirtualEntity")]
        public string SourceVirtualEntityFk { get; set; }
        public virtual VirtualEntity SourceVirtualEntity { get; set; }

        [ForeignKey("DestinationVirtualEntity")]
        public string DestinationVirtualEntityFk { get; set; }
        public virtual VirtualEntity DestinationVirtualEntity { get; set; }

        [ForeignKey("User")]
        [Required]
        public int UserFk { get; set; }
        public virtual IotUser User { get; set; }
    }
}
