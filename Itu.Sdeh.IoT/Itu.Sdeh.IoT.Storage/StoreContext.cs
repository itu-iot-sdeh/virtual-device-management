﻿using Itu.Sdeh.IoT.Storage.Models.Entities;
using Itu.Sdeh.IoT.Storage.Models.Entities.Credentials;
using Itu.Sdeh.IoT.Storage.Models.Events;
using Itu.Sdeh.IoT.Storage.Models.Users;
using Microsoft.EntityFrameworkCore;

namespace Itu.Sdeh.IoT.Storage
{
    public class StoreContext: DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=IotItuSdehA;Trusted_Connection=True;");
            optionsBuilder.UseSqlServer(@"Server=192.168.99.100,32773;Database=IotItuSdehB;Integrated Security=False;User ID=sa;Password=SecretAdmin_Paper!");
        }

        public DbSet<PhysicalEntity> PhysicalEntities { get; set; }
        public DbSet<VirtualEntity> VirtualEntities { get; set; }
        public DbSet<DeviceConfigured> DeviceConfigurationEvents { get; set; }
        public DbSet<WatsonDeviceCredential> WatsonDeviceCredentials { get; set; }
        public DbSet<IotUser> IotUsers { get; set; }
    }
}
