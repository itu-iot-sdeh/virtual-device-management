﻿using System.Threading.Tasks;
using Itu.Sdeh.IoT.Storage;
using Itu.Sdeh.IoT.Web.Services.DTOs;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Itu.Sdeh.IoT.Web.Services.Queries.Device
{
    public class GetPhysicalDeviceEventsByPhysical : IRequest<PhysicalDeviceDto>
    {
        public string HardwareId { get; set; }
    }

    public class GetPhysicalDeviceEventsByPhysicalHandler: IAsyncRequestHandler<GetPhysicalDeviceEventsByPhysical, PhysicalDeviceDto>
    {
        private readonly StoreContext _store;

        public GetPhysicalDeviceEventsByPhysicalHandler(StoreContext store)
        {
            _store = store;
        }

        public Task<PhysicalDeviceDto> Handle(GetPhysicalDeviceEventsByPhysical message)
            => _store
                .PhysicalEntities
                .FirstAsync(pe => pe.HardwareId == message.HardwareId)
                .ContinueWith(t => new PhysicalDeviceDto
                {
                    HardwareId = t.Result.HardwareId
                });
    }
}
