﻿using System.Linq;
using System.Threading.Tasks;
using Itu.Sdeh.IoT.Storage;
using Itu.Sdeh.IoT.Web.Services.DTOs;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Itu.Sdeh.IoT.Web.Services.Queries.Device
{
    public class GetVirtualDeviceEventsByVirtual: IRequest<VirtualDeviceEventsDto>
    {
        public string VirtualDeviceId { get; set; }
        public string DeviceType { get; set; }
    }

    public class GetVirtualDeviceEventsByVirtualHandler: IAsyncRequestHandler<GetVirtualDeviceEventsByVirtual, VirtualDeviceEventsDto>
    {
        private readonly StoreContext _store;

        public GetVirtualDeviceEventsByVirtualHandler(StoreContext store)
        {
            _store = store;
        }

        public async Task<VirtualDeviceEventsDto> Handle(GetVirtualDeviceEventsByVirtual message)
        {
            var virtualDevice = await _store
                .VirtualEntities
                .Include(ve => ve.SourceConfigurationEvents)
                .Include(ve => ve.DestinationConfigurationEvents)
                .FirstAsync(ve => ve.VirtualId == message.VirtualDeviceId && ve.DeviceType == message.DeviceType);

            return new VirtualDeviceEventsDto
            {
                DestinationEvents = virtualDevice
                    .DestinationConfigurationEvents
                    .Select(e => new ConfiguredDto
                    {
                        DestinationVirtualEntityFk = e.DestinationVirtualEntityFk,
                        Id = e.Id,
                        PhysicalEntityFk = e.PhysicalEntityFk,
                        SourceVirtualEntityFk = e.SourceVirtualEntityFk,
                        TimeOfEvent = e.TimeOfEvent,
                        UserFk = e.UserFk
                    }).ToArray(),
                SourceEvents = virtualDevice
                    .SourceConfigurationEvents
                    .Select(e => new ConfiguredDto
                    {
                        DestinationVirtualEntityFk = e.DestinationVirtualEntityFk,
                        Id = e.Id,
                        PhysicalEntityFk = e.PhysicalEntityFk,
                        SourceVirtualEntityFk = e.SourceVirtualEntityFk,
                        TimeOfEvent = e.TimeOfEvent,
                        UserFk = e.UserFk
                    }).ToArray()
            };
        }
    }
}
