﻿using System.Linq;
using System.Threading.Tasks;
using Itu.Sdeh.IoT.Storage;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Itu.Sdeh.IoT.Web.Services.Queries.Device
{
    public class GetVirtualDeviceIdByPhysical: IRequest<string>
    {
        public string HardwareId { get; set; }
    }

    public class GetVirtualDeviceIdByPhysicalHandler: IAsyncRequestHandler<GetVirtualDeviceIdByPhysical, string>
    {
        private readonly StoreContext _store;

        public GetVirtualDeviceIdByPhysicalHandler(StoreContext store)
        {
            _store = store;
        }

        public Task<string> Handle(GetVirtualDeviceIdByPhysical message)
            => _store
                .PhysicalEntities
                .FirstAsync(pe => pe.HardwareId == message.HardwareId)
                .ContinueWith(t => t.Result.VirtualEntityFk);
    }
}

