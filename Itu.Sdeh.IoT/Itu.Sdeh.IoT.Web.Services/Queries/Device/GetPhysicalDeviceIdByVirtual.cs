﻿using System.Threading.Tasks;
using Itu.Sdeh.IoT.Storage;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Itu.Sdeh.IoT.Web.Services.Queries.Device
{
    public class GetPhysicalDeviceIdByVirtual: IRequest<string>
    {
        public string VirtualDeviceId { get; set; }
        public string DeviceType { get; set; }
    }

    public class GetPhysicalDeviceIdByVirtualHandler : IAsyncRequestHandler<GetPhysicalDeviceIdByVirtual, string>
    {
        private readonly StoreContext _store;

        public GetPhysicalDeviceIdByVirtualHandler(StoreContext store)
        {
            _store = store;
        }
        public Task<string> Handle(GetPhysicalDeviceIdByVirtual message)
            => _store
                .VirtualEntities
                .Include(ve => ve.PhysicalEntity)
                .FirstAsync(ve => ve.VirtualId == message.VirtualDeviceId && ve.DeviceType == message.DeviceType)
                .ContinueWith(t => t.Result.PhysicalEntity.HardwareId);
    }
}
