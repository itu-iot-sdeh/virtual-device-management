﻿using System.Threading.Tasks;
using Itu.Sdeh.IoT.Storage;
using Itu.Sdeh.IoT.Storage.Models.Users;
using MediatR;

namespace Itu.Sdeh.IoT.Web.Services.Commands.User
{
    public class AddNewUser: IRequest<int>
    {
        //TODO: Actually implement
    }

    public class AddNewUserHandler : IAsyncRequestHandler<AddNewUser, int>
    {
        private readonly StoreContext _store;
        public AddNewUserHandler(StoreContext store)
        {
            _store = store;
        }

        public async Task<int> Handle(AddNewUser message)
        {
            var user = new IotUser();
            var added = await _store.IotUsers.AddAsync(user);
            await _store.SaveChangesAsync();

            return added.Entity.Id;
        }
    }
}
