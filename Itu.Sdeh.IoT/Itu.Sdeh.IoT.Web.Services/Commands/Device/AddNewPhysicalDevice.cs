﻿using System;
using System.Threading.Tasks;
using Itu.Sdeh.IoT.Storage;
using Itu.Sdeh.IoT.Storage.Models.Entities;
using Itu.Sdeh.IoT.Storage.Models.Entities.Credentials;
using Itu.Sdeh.IoT.Storage.Models.Events;
using Itu.Sdeh.IoT.Watson;
using Itu.Sdeh.IoT.Watson.Request;
using Itu.Sdeh.IoT.Web.Services.DTOs;
using MediatR;

namespace Itu.Sdeh.IoT.Web.Services.Commands.Device
{
    public class AddNewPhysicalDevice: IRequest<WatsonDeviceDto>
    {
        public string HardwareId { get; set; }
        public string DeviceType { get; set; }
        public int UserId { get; set; }
    }

    public class AddNewPhysicalDeviceHandler: IAsyncRequestHandler<AddNewPhysicalDevice, WatsonDeviceDto>
    {
        private readonly StoreContext _storeContext;
        private readonly WatsonFacade _watsonFacade;

        public AddNewPhysicalDeviceHandler(StoreContext storeContext, WatsonFacade watsonFacade)
        {
            _storeContext = storeContext;
            _watsonFacade = watsonFacade;
        }

        public async Task<WatsonDeviceDto> Handle(AddNewPhysicalDevice message)
        {
            //TODO: Check hardware id does not already exists and is not null

            //Create in watson
            var newDevice = await _watsonFacade
                .AddDevice(new DeviceAdditionRequest
                {
                    DeviceId = Guid.NewGuid().ToString(),
                    DeviceType = message.DeviceType
                });

            //Create in entity store
            var credentials = new WatsonDeviceCredential
            {
                AuthenticationToken = newDevice.AuthenticationToken,
                OrganizationId = newDevice.OrganizationId
            };
            await _storeContext.WatsonDeviceCredentials.AddAsync(credentials);

            var virtualEntity = new VirtualEntity
            {
                DeviceType = newDevice.DeviceType,
                VirtualId = newDevice.DeviceId,
                WatsonCredential = credentials
            };
            await _storeContext.VirtualEntities.AddAsync(virtualEntity);

            var physicalEntity = new PhysicalEntity
            {
                HardwareId = message.HardwareId,
                VirtualEntity = virtualEntity
            };
            await _storeContext.PhysicalEntities.AddAsync(physicalEntity);

            //Add event
            var eventGuid = Guid.NewGuid().ToString();
            var addedEvent = new DeviceConfigured
            {
                DestinationVirtualEntity = virtualEntity,
                PhysicalEntity = physicalEntity,
                TimeOfEvent = DateTime.UtcNow,
                EventGuid = eventGuid,
                UserFk = message.UserId
            };
            await _storeContext.DeviceConfigurationEvents.AddAsync(addedEvent);

            await _storeContext.SaveChangesAsync();

            //Return new device credentials
            return new WatsonDeviceDto
            {
                AuthenticationToken = newDevice.AuthenticationToken,
                DeviceId = newDevice.DeviceId,
                DeviceType = newDevice.DeviceType,
                OrganizationId = newDevice.OrganizationId
            };
        }
    }
}
