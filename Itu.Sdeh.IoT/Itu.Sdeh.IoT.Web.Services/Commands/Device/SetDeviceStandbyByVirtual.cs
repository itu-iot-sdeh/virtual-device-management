﻿using System;
using System.Threading.Tasks;
using Itu.Sdeh.IoT.Storage;
using Itu.Sdeh.IoT.Watson;
using Itu.Sdeh.IoT.Watson.Request;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Itu.Sdeh.IoT.Web.Services.Commands.Device
{
    public class SetDeviceStandbyByVirtual : IRequest
    {
        public string DeviceId { get; set; }
        public string TypeId { get; set; }
        public bool IsStandbyValue { get; set; }
    }

    public class SetDeviceStandbyByVirtualHandler : IAsyncRequestHandler<SetDeviceStandbyByVirtual>
    {
        private readonly StoreContext _storeContext;
        private readonly WatsonFacade _watsonFacade;

        public SetDeviceStandbyByVirtualHandler(StoreContext storeContext, WatsonFacade watsonFacade)
        {
            _storeContext = storeContext;
            _watsonFacade = watsonFacade;
        }

        public async Task Handle(SetDeviceStandbyByVirtual message)
        {
            //TODO Check virtual entity exists

            var responseCode = await _watsonFacade.RemoveDevice(new DeviceRemovalRequest
            {
                DeviceId = message.DeviceId,
                TypeId = message.TypeId,
                OnlySetStandby = true
            });

            if (!responseCode.WasSucces)
                throw new Exception(responseCode.Code + responseCode.Message);

            var virtualEntity = await _storeContext
                .VirtualEntities
                .FirstAsync(ve => ve.VirtualId == message.DeviceId && ve.DeviceType == message.TypeId);

            virtualEntity.IsStandby = message.IsStandbyValue;

            await _storeContext.SaveChangesAsync();
        }
    }
}
