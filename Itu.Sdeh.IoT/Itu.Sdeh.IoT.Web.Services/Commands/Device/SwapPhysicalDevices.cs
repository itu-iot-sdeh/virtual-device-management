﻿using System;
using System.Threading.Tasks;
using Itu.Sdeh.IoT.Storage;
using Itu.Sdeh.IoT.Storage.Models.Entities;
using Itu.Sdeh.IoT.Storage.Models.Events;
using Itu.Sdeh.IoT.Watson;
using Itu.Sdeh.IoT.Watson.Models;
using Itu.Sdeh.IoT.Watson.Request;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Itu.Sdeh.IoT.Web.Services.Commands.Device
{
    public class SwapPhysicalDevices: IRequest
    {
        public string DeviceAId { get; set; }
        public string DeviceATypeId { get; set; }
        public string DeviceBId { get; set; }
        public string DeviceBTypeId { get; set; }
        public bool IgnoreNoAcknowledgementFromB { get; set; }
        public int UserId { get; set; }
    }

    public class SwapPhysicalDevicesHandler : IAsyncRequestHandler<SwapPhysicalDevices>
    {
        private readonly StoreContext _storeContext;
        private readonly WatsonFacade _watsonFacade;

        public SwapPhysicalDevicesHandler(StoreContext storeContext, WatsonFacade watsonFacade)
        {
            _storeContext = storeContext;
            _watsonFacade = watsonFacade;
        }

        public async Task Handle(SwapPhysicalDevices message)
        {
            var oldDeviceA = await _storeContext
                .VirtualEntities
                .Include(ve => ve.WatsonCredential)
                .Include(ve => ve.PhysicalEntity)
                .FirstAsync(ve => ve.VirtualId == message.DeviceAId && ve.DeviceType == message.DeviceATypeId);
            var oldDeviceB = await _storeContext
                .VirtualEntities
                .Include(ve => ve.WatsonCredential)
                .Include(ve => ve.PhysicalEntity)
                .FirstAsync(ve => ve.VirtualId == message.DeviceBId && ve.DeviceType == message.DeviceBTypeId);

            //Create device configs
            var deviceAConfig = new WatsonDevice
            {
                AuthenticationToken = oldDeviceA.WatsonCredential.AuthenticationToken,
                DeviceId = oldDeviceA.VirtualId,
                DeviceType = oldDeviceA.DeviceType,
                OrganizationId = oldDeviceA.WatsonCredential.OrganizationId
            };
            var deviceBConfig = new WatsonDevice
            {
                AuthenticationToken = oldDeviceB.WatsonCredential.AuthenticationToken,
                DeviceId = oldDeviceB.VirtualId,
                DeviceType = oldDeviceB.DeviceType,
                OrganizationId = oldDeviceB.WatsonCredential.OrganizationId
            };

            //Temp new device
            var deviceGuid = Guid.NewGuid().ToString();
            var tempDeviceConfig = await _watsonFacade
                .AddDevice(new DeviceAdditionRequest
                {
                    DeviceId = deviceGuid,
                    DeviceType = WatsonFacade.TempDeviceType
                });

            try
            {
                //Reconfigure A -> Temp
                var reponse = await _watsonFacade.ReconfigureDeviceAwaitAcknowledgement(new DeviceReconfigureRequest
                {
                    DeviceId = message.DeviceAId,
                    TypeId = message.DeviceATypeId,
                    NewDeviceStatus = DeviceStatus.Inactive,
                    NewDeviceConfiguration = tempDeviceConfig
                });

                if (!reponse.WasSucces)
                    throw new Exception("Failed reconfigure A -> Temp with: " + reponse.Code + reponse.Message);

                try
                {
                    //Reconfigure B -> A
                    reponse = await _watsonFacade.ReconfigureDeviceAwaitAcknowledgement(new DeviceReconfigureRequest
                    {
                        DeviceId = message.DeviceBId,
                        TypeId = message.DeviceBTypeId,
                        NewDeviceStatus = DeviceStatus.Active,
                        NewDeviceConfiguration = deviceAConfig
                    });

                    if (!reponse.WasSucces && !message.IgnoreNoAcknowledgementFromB) //If we should ignore missing acknowledgement from B, we proceed
                        throw new Exception("Failed reconfigure B -> A with: " + reponse.Code + reponse.Message);

                    try
                    {
                        //Reconfigure Temp -> B
                        reponse = await _watsonFacade.ReconfigureDeviceAwaitAcknowledgement(new DeviceReconfigureRequest
                        {
                            DeviceId = tempDeviceConfig.DeviceId,
                            TypeId = tempDeviceConfig.DeviceType,
                            NewDeviceStatus = DeviceStatus.Active,
                            NewDeviceConfiguration = deviceBConfig
                        });

                        if (!reponse.WasSucces)
                            throw new Exception("Failed reconfigure Temp -> A with: " + reponse.Code + reponse.Message);

                        try
                        {
                            //Swap was succesful in Watson!
                            //Persist swap to store
                            //Temp
                            using (var dbContextTransaction = _storeContext.Database.BeginTransaction())
                            {
                                try
                                {
                                    var tempVirtual = new VirtualEntity
                                    {
                                        DeviceType = tempDeviceConfig.DeviceType,
                                        IsStandby = true,
                                        VirtualId = tempDeviceConfig.DeviceId
                                    };
                                    await _storeContext.VirtualEntities.AddAsync(tempVirtual);
                                    await _storeContext.SaveChangesAsync();

                                    //A -> Temp
                                    var oldPhysicalA = oldDeviceA.PhysicalEntity;
                                    var oldPhysicalB = oldDeviceB.PhysicalEntity;
                                    oldPhysicalA.VirtualEntity = tempVirtual;
                                    await _storeContext.SaveChangesAsync();

                                    //B -> A
                                    oldPhysicalB.VirtualEntity = oldDeviceA;
                                    await _storeContext.SaveChangesAsync();

                                    //Temp -> B
                                    oldPhysicalA.VirtualEntity = oldDeviceB;
                                    await _storeContext.SaveChangesAsync();

                                    //Delete Temp
                                    _storeContext.VirtualEntities.Remove(tempVirtual);
                                    await _storeContext.SaveChangesAsync();

                                    //Device configured event (same guid)
                                    var eventGuid = Guid.NewGuid().ToString();
                                    _storeContext.DeviceConfigurationEvents.Add(new DeviceConfigured
                                    {
                                        DestinationVirtualEntity = oldDeviceB,
                                        SourceVirtualEntity = oldDeviceA,
                                        PhysicalEntity = oldPhysicalA,
                                        TimeOfEvent = DateTime.UtcNow,
                                        EventGuid = eventGuid,
                                        UserFk = message.UserId
                                    });
                                    _storeContext.DeviceConfigurationEvents.Add(new DeviceConfigured
                                    {
                                        DestinationVirtualEntity = oldDeviceA,
                                        SourceVirtualEntity = oldDeviceB,
                                        PhysicalEntity = oldPhysicalB,
                                        TimeOfEvent = DateTime.UtcNow,
                                        EventGuid = eventGuid,
                                        UserFk = message.UserId
                                    });

                                    //Save
                                    await _storeContext.SaveChangesAsync();
                                    dbContextTransaction.Commit();
                                }
                                catch
                                {
                                    dbContextTransaction.Rollback();
                                    throw;
                                }
                            }
                        }
                        catch
                        {
                            //Failed persisting to store,
                            //Must revert B -> Temp
                            reponse = await _watsonFacade.ReconfigureDeviceAwaitAcknowledgement(new DeviceReconfigureRequest
                            {
                                DeviceId = deviceBConfig.DeviceId,
                                TypeId = deviceBConfig.DeviceType,
                                NewDeviceStatus = DeviceStatus.Inactive,
                                NewDeviceConfiguration = tempDeviceConfig
                            });

                            if (!reponse.WasSucces)
                                throw new Exception("Failed reconfigure recovery B -> Temp with: " + reponse.Code + reponse.Message);

                            throw;
                        }

                    }
                    catch
                    {
                        //Temp -> B Failed,
                        //Must revert A -> B
                        reponse = await _watsonFacade.ReconfigureDeviceAwaitAcknowledgement(new DeviceReconfigureRequest
                        {
                            DeviceId = deviceAConfig.DeviceId,
                            TypeId = deviceAConfig.DeviceType,
                            NewDeviceStatus = DeviceStatus.Active,
                            NewDeviceConfiguration = deviceBConfig
                        });

                        if (!reponse.WasSucces && !message.IgnoreNoAcknowledgementFromB) //If we should ignore missing acknowledgement from B, we proceed
                            throw new Exception("Failed reconfigure recovery A -> B with: " + reponse.Code + reponse.Message);

                        throw;
                    }
                }
                catch
                {
                    //B -> A Failed,
                    //Must revert Temp -> A
                    reponse = await _watsonFacade.ReconfigureDeviceAwaitAcknowledgement(new DeviceReconfigureRequest
                    {
                        DeviceId = tempDeviceConfig.DeviceId,
                        TypeId = tempDeviceConfig.DeviceType,
                        NewDeviceStatus = DeviceStatus.Active,
                        NewDeviceConfiguration = deviceAConfig
                    });

                    if (!reponse.WasSucces)
                        throw new Exception("Failed reconfigure recovery Temp -> A with: " + reponse.Code + reponse.Message);

                    throw;
                }
            }
            finally 
            {
                //Delete watson temp device
                await _watsonFacade.RemoveDevice(new DeviceRemovalRequest
                {
                    DeviceId = tempDeviceConfig.DeviceId,
                    OnlySetStandby = false,
                    TypeId = tempDeviceConfig.DeviceType
                });
            }
        }
    }
}
