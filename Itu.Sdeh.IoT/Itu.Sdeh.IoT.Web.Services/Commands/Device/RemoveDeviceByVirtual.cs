﻿using System;
using System.Threading.Tasks;
using Itu.Sdeh.IoT.Storage;
using Itu.Sdeh.IoT.Watson;
using Itu.Sdeh.IoT.Watson.Request;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Itu.Sdeh.IoT.Web.Services.Commands.Device
{
    public class RemoveDeviceByVirtual: IRequest
    {
        public string DeviceId { get; set; }
        public string TypeId { get; set; }
    }

    public class RemoveDeviceByVirtualHandler : IAsyncRequestHandler<RemoveDeviceByVirtual>
    {
        private readonly StoreContext _storeContext;
        private readonly WatsonFacade _watsonFacade;

        public RemoveDeviceByVirtualHandler(StoreContext storeContext, WatsonFacade watsonFacade)
        {
            _storeContext = storeContext;
            _watsonFacade = watsonFacade;
        }

        public async Task Handle(RemoveDeviceByVirtual message)
        {
            //TODO Check virtual entity exists

            var responseCode = await _watsonFacade.RemoveDevice(new DeviceRemovalRequest
            {
                DeviceId = message.DeviceId,
                TypeId = message.TypeId,
                OnlySetStandby = false
            });

            if (!responseCode.WasSucces)
                throw new Exception(responseCode.Code + responseCode.Message);

            var virtualEntity = await _storeContext
                .VirtualEntities
                .FirstAsync(ve => ve.VirtualId == message.DeviceId && ve.DeviceType == message.TypeId);

            _storeContext.VirtualEntities.Remove(virtualEntity);
            await _storeContext.SaveChangesAsync();
        }
    }
}
