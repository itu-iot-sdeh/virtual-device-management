﻿namespace Itu.Sdeh.IoT.Web.Services.DTOs
{
    public class PhysicalDeviceDto
    {
        public string HardwareId { get; set; }
    }
}
