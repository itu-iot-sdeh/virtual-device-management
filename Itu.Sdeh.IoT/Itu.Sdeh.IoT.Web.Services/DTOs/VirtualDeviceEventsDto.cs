﻿using System;

namespace Itu.Sdeh.IoT.Web.Services.DTOs
{
    public class VirtualDeviceEventsDto
    {
        public ConfiguredDto[] SourceEvents { get; set; }
        public ConfiguredDto[] DestinationEvents { get; set; }
    }

    public class ConfiguredDto
    {
        public int Id { get; set; }

        public DateTime TimeOfEvent { get; set; }

        public string PhysicalEntityFk { get; set; }

        public string SourceVirtualEntityFk { get; set; }

        public string DestinationVirtualEntityFk { get; set; }
        public int UserFk { get; set; }
    }
}
