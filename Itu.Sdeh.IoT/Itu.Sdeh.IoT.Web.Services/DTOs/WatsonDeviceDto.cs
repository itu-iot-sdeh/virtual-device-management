﻿namespace Itu.Sdeh.IoT.Web.Services.DTOs
{
    public class WatsonDeviceDto
    {
        public string DeviceId { get; set; }
        public string DeviceType { get; set; }
        public string OrganizationId { get; set; }
        public string AuthenticationToken { get; set; }
    }
}
