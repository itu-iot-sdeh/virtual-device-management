﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Itu.Sdeh.IoT.Watson.Extensions;
using Itu.Sdeh.IoT.Watson.Models;
using Itu.Sdeh.IoT.Watson.Reponse;
using Itu.Sdeh.IoT.Watson.Request;
using M2Mqtt;
using Newtonsoft.Json;

namespace Itu.Sdeh.IoT.Watson
{
    public class WatsonFacade
    {
        public const string TempDeviceType = "TEMP_SWAP";

        private const string DeviceAcknowledgeConfiguration = "config-reckon";
        private const string DeviceConfigurationCommand = "config-device";

        private const string ClientIdDelimiter = ":";
        private const string Domain = ".messaging.internetofthings.ibmcloud.com";
        private const int MqttsPort = 8883;

        private string ApiBaseUrl => @"https://" + _orgId + ".internetofthings.ibmcloud.com/api/v0002/";

        private readonly string _orgId;
        private readonly string _apiKey;
        private readonly string _authToken;

        public WatsonFacade(string organizationId, string apiKey, string authToken)
        {
            _orgId = organizationId;
            _apiKey = apiKey;
            _authToken = authToken;
        }

        private HttpClient GetWatsonRestClient()
        {
            var credentials = Encoding.ASCII.GetBytes(_apiKey + ":" + _authToken);
            var handler = new HttpClientHandler
            {
                PreAuthenticate = true,
                Credentials = new NetworkCredential(_apiKey, _authToken)
            };
            var client = new HttpClient(handler, true)
            {
                Timeout = TimeSpan.FromSeconds(100),
                BaseAddress = new Uri(ApiBaseUrl)
            };
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials));
            return client;
        }

        public MqttClient GetConnectedWatsonMqttClient() //TODO: set to private
        {
            var hostName = _orgId + Domain;
            var appId = "device-config-app";
            var client = new MqttClient(hostName, MqttsPort, true, new X509Certificate2(), new X509Certificate2(), MqttSslProtocols.TLSv1_2);
            client.Connect("a" + ClientIdDelimiter + _orgId + ClientIdDelimiter + appId, _apiKey, _authToken);
            return client;
        }

        public async Task<WatsonDevice> AddDevice(DeviceAdditionRequest deviceAdditionRequest)
        {
            Console.WriteLine("DeviceAdditionRequest: " + deviceAdditionRequest.ToJson());

            using (var client = GetWatsonRestClient())
            {
                var typeId = deviceAdditionRequest.DeviceType;
                var deviceId = deviceAdditionRequest.DeviceId;
                var response = await client.PostAsync(
                    $"device/types/{typeId}/devices",
                    new JsonContent(new { deviceId })
                );

                if(response.StatusCode != HttpStatusCode.Created)
                    throw new Exception(response.StatusCode + " " +response.ReasonPhrase);

                var content = await response.Content.ReadAsStringAsync();
                var jsonResponse = JsonConvert.DeserializeObject<dynamic>(content);

                return new WatsonDevice
                {
                    AuthenticationToken = jsonResponse.authToken,
                    DeviceId = jsonResponse.deviceId,
                    DeviceType = jsonResponse.typeId,
                    OrganizationId = _orgId
                };
            }
        }

        public async Task<ResponseCode> RemoveDevice(DeviceRemovalRequest deviceRemovalRequest)
        {
            Console.WriteLine("DeviceRemovalRequest: " + deviceRemovalRequest.ToJson());

            //TODO: MQTT: Tell device that it is removed / standby (await?)

            //If standby only, return success
            if (deviceRemovalRequest.OnlySetStandby)
                return new ResponseCode
                {
                    Code = (int)HttpStatusCode.NoContent,
                    WasSucces = true
                };

            //Remove
            using (var client = GetWatsonRestClient())
            {
                var typeId = deviceRemovalRequest.TypeId;
                var deviceId = deviceRemovalRequest.DeviceId;

                var response = await client.DeleteAsync($"device/types/{typeId}/devices/{deviceId}");
                return new ResponseCode
                {
                    Code = (int)response.StatusCode,
                    WasSucces = response.StatusCode == HttpStatusCode.NoContent,
                    Message = response.ReasonPhrase
                };
            }
        }

        public Task<object> LastDeviceEvent(DeviceLastEventRequest deviceLastEventRequest)
        {
            throw new NotImplementedException();
        }

        //TODO: Implement timeout? Could take like half an hour...
        public async Task<ResponseCode> ReconfigureDeviceAwaitAcknowledgement(DeviceReconfigureRequest deviceReconfigureRequest)
        {
            Console.WriteLine("DeviceReconfigureRequest: " + deviceReconfigureRequest.ToJson());

            var client = GetConnectedWatsonMqttClient();
            try
            {

                //Task related
                var waitTime = TimeSpan.FromMinutes(2); //TODO: Set realistic time
                var invalidAfter = DateTime.UtcNow + waitTime;
                var tcs = new TaskCompletionSource<bool>();

                //Subscribe response event
                var responseType = deviceReconfigureRequest.NewDeviceConfiguration.DeviceType;
                var responseId = deviceReconfigureRequest.NewDeviceConfiguration.DeviceId;
                var responseTopics = new[] {$"iot-2/type/{responseType}/id/{responseId}/evt/+/fmt/+"};
                client.Subscribe(responseTopics, new byte[] {2});
                client.MqttMsgPublishReceived += (sender, args) =>
                {
                    //For the moment if any response comes on this type+id, we accept it
                       tcs.TrySetResult(true);
                };
                var ct =
                    new CancellationTokenSource((int) (waitTime.TotalMilliseconds * 1.1)); // * 1.1 in order to overlap the invalidAfter
                ct.Token.Register(() => tcs.TrySetCanceled(), false);

                //Publish
                var publishType = deviceReconfigureRequest.TypeId;
                var publishId = deviceReconfigureRequest.DeviceId;
                var publishTopic = $"iot-2/type/{publishType}/id/{publishId}/cmd/{DeviceConfigurationCommand}/fmt/json";
                var publishMessage = new DeviceConfigurationMessage
                {
                    AuthenticationToken = deviceReconfigureRequest.NewDeviceConfiguration.AuthenticationToken,
                    DeviceType = deviceReconfigureRequest.NewDeviceConfiguration.DeviceType,
                    OrganizationId = deviceReconfigureRequest.NewDeviceConfiguration.OrganizationId,
                    VirtualId = deviceReconfigureRequest.NewDeviceConfiguration.DeviceId,
                    DeviceStatus = deviceReconfigureRequest.NewDeviceStatus,
                    InvalidAfter = invalidAfter
                };
                client.Publish(publishTopic, Encoding.UTF8.GetBytes(publishMessage.ToJson()), 2, false);

                //Await response event
                var result = await tcs.Task;
                return new ResponseCode
                {
                    WasSucces = result,
                    Code = (int) (result ? HttpStatusCode.NoContent : HttpStatusCode.RequestTimeout)
                };
            }
            catch (Exception e)
            {
                return new ResponseCode
                {
                    WasSucces = false,
                    Code = (int) HttpStatusCode.InternalServerError,
                    Message = e.ToString()
                };
            }
            finally
            {
                client.Disconnect();
            }
        }
    }
}
