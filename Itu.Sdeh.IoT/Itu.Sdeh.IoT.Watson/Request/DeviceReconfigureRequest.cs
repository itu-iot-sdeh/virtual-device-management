﻿using Itu.Sdeh.IoT.Watson.Models;

namespace Itu.Sdeh.IoT.Watson.Request
{
    public class DeviceReconfigureRequest
    {
        public string TypeId { get; set; }
        public string DeviceId { get; set; }
        public DeviceStatus NewDeviceStatus { get; set; }
        public WatsonDevice NewDeviceConfiguration { get; set; }
    }
}
