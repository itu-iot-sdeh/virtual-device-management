﻿namespace Itu.Sdeh.IoT.Watson.Request
{
    public class DeviceRemovalRequest
    {
        public string TypeId { get; set; }
        public string DeviceId { get; set; }
        public bool OnlySetStandby { get; set; }
    }
}
