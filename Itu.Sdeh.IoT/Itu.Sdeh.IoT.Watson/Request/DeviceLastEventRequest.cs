﻿namespace Itu.Sdeh.IoT.Watson.Request
{
    public class DeviceLastEventRequest
    {
        public string TypeId { get; set; }
        public string DeviceId { get; set; }
    }
}
