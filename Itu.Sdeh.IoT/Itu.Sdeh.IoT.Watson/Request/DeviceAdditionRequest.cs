﻿namespace Itu.Sdeh.IoT.Watson.Request
{
    public class DeviceAdditionRequest
    {
        public string DeviceType { get; set; }

        public string DeviceId { get; set; }

        //TODO: authtoken
        //TODO: deviceInfo
        //TODO: location
        //TODO: metadata
    }
}
