﻿namespace Itu.Sdeh.IoT.Watson.Reponse
{
    public class ResponseCode
    {
        public bool WasSucces { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
    }
}
