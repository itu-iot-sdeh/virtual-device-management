﻿using System.Net.Http;
using System.Text;
using Newtonsoft.Json;

namespace Itu.Sdeh.IoT.Watson.Extensions
{
    public static class ToJsonExtension
    {
        public static string ToJson(this object obj)
            => JsonConvert.SerializeObject(obj);

        public static JsonContent ToJsonContent(this object obj)
            => new JsonContent(obj);
    }

    public class JsonContent : StringContent
    {
        public JsonContent(object obj) :
            base(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json")
        { }
    }
}
