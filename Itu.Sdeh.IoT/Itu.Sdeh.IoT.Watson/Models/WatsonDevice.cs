﻿namespace Itu.Sdeh.IoT.Watson.Models
{
    public class WatsonDevice
    {
        public string DeviceId { get; set; }
        public string DeviceType { get; set; }
        public string OrganizationId { get; set; }
        public string AuthenticationToken { get; set; }
    }
}
