﻿using System;

namespace Itu.Sdeh.IoT.Watson.Models
{
    internal class DeviceConfigurationMessage
    {
        public DateTime InvalidAfter { get; set; }
        public string OrganizationId { get; set; }
        public string AuthenticationToken { get; set; }
        public string DeviceType { get; set; }
        public string VirtualId { get; set; }
        public DeviceStatus DeviceStatus { get; set; }
    }
}
