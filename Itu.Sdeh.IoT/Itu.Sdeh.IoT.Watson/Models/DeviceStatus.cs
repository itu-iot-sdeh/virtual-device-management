﻿namespace Itu.Sdeh.IoT.Watson.Models
{
    public enum DeviceStatus
    {
        Active = 1,
        Inactive = 0,
    }
}
