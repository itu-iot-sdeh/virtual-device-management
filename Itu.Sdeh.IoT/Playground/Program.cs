﻿using System.Diagnostics;
using System.Threading.Tasks;
using Itu.Sdeh.IoT.Storage;
using Itu.Sdeh.IoT.Watson;
using Itu.Sdeh.IoT.Web.Services.Commands.Device;
using Itu.Sdeh.IoT.Web.Services.Commands.User;
using Itu.Sdeh.IoT.Web.Services.Queries.Device;

namespace Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            AsyncMain().Wait();
        }

        public static async Task AsyncMain()
        {
            #region Test 1

            //These should be hidden and be managed inside the Itu.Sdeh.IoT.Web.Services by using dependency injection and mediator.
            //They are here for ease of configuring the example and readability of the code.
            var watson = new WatsonFacade("etfx6k", "a-etfx6k-lwuaeyorhm", "lqI9nHMN3Pu@-K5+2K");
            var store = new StoreContext();
            store.Database.EnsureDeleted();
            store.Database.EnsureCreated();

            //Add users
            var userId = await new AddNewUserHandler(store).Handle(new AddNewUser());

            //Add three devices
            var device1 = await new AddNewPhysicalDeviceHandler(store, watson).Handle(new AddNewPhysicalDevice
            {
                DeviceType = "Device-type-1",
                HardwareId = "hw_d_1",
                UserId = userId
            });
            var device2 = await new AddNewPhysicalDeviceHandler(store, watson).Handle(new AddNewPhysicalDevice
            {
                DeviceType = "Device-type-2",
                HardwareId = "hw_d_2",
                UserId = userId
            });
            var device3 = await new AddNewPhysicalDeviceHandler(store, watson).Handle(new AddNewPhysicalDevice
            {
                DeviceType = "Device-type-3",
                HardwareId = "hw_d_3",
                UserId = userId
            });

			//Simulate the transfering of these credentials to hardware
			//Breaks the debugger while manually moving them
			///Wait 2 minutes for the measurements in the dashboard to be visible
			Debugger.Break();

            //Get physical for device 1 before swap
            var phys1PreSwap = await new GetPhysicalDeviceIdByVirtualHandler(store).Handle(new GetPhysicalDeviceIdByVirtual
            {
                DeviceType = device1.DeviceType,
                VirtualDeviceId = device1.DeviceId
            });

            //Swap device 1 and 3 (Simulates a replacement of device 1 with device 3).
            await new SwapPhysicalDevicesHandler(store, watson).Handle(new SwapPhysicalDevices
            {
                DeviceAId = device1.DeviceId,
                DeviceATypeId = device1.DeviceType,
                DeviceBId = device3.DeviceId,
                DeviceBTypeId = device3.DeviceType,
                UserId = userId
            });

            //Get physical for device 1 after swap
            var phys1PostSwap = await new GetPhysicalDeviceIdByVirtualHandler(store).Handle(new GetPhysicalDeviceIdByVirtual
            {
                DeviceType = device1.DeviceType,
                VirtualDeviceId = device1.DeviceId
            });

            //Compare phys1PreSwap and phys1PostSwap, should not be identical
            Debug.Assert(phys1PreSwap != phys1PostSwap);
			//Check that the received data has been swapped in Watson IoT backend
			///Wait 2 minutes for the measurements in the dashboard to be visible
			Debugger.Break();

            //Swap device 1 and 3 back (Simulate that a device was swapped instead of simple "new" addition).
            await new SwapPhysicalDevicesHandler(store, watson).Handle(new SwapPhysicalDevices
            {
                DeviceAId = device1.DeviceId,
                DeviceATypeId = device1.DeviceType,
                DeviceBId = device3.DeviceId,
                DeviceBTypeId = device3.DeviceType,
                UserId = userId
            });

            //Get the physical for device 1 after swapping back
            var phys1PostSwap2 = await new GetPhysicalDeviceIdByVirtualHandler(store).Handle(new GetPhysicalDeviceIdByVirtual
            {
                DeviceType = device1.DeviceType,
                VirtualDeviceId = device1.DeviceId
            });

            //Ensure that phys1PreSwap and phys1PostSwap2 are identical
            Debug.Assert(phys1PreSwap == phys1PostSwap2);
			//Check that the received data has been swapped in Watson IoT backend
			//Wait 2 minutes for the measurements in the dashboard to be visible
			Debugger.Break();

            //Get events for device 3
            var device3Events = await new GetVirtualDeviceEventsByVirtualHandler(store).Handle(new GetVirtualDeviceEventsByVirtual
            {
                DeviceType = device3.DeviceType,
                VirtualDeviceId = device3.DeviceId
            });

            //Remove device 2
            await new RemoveDeviceByVirtualHandler(store, watson).Handle(new RemoveDeviceByVirtual
            {
                TypeId = device2.DeviceType,
                DeviceId = device2.DeviceId
            });

			//Check that the device has been removed from Watson IoT backend
			//Wait 2 minutes for the measurements in the dashboard to be visible
			Debugger.Break();

			#endregion
			#region Test 2

			//Simulate that a device breaks down (shutdown the wifi module).
			///Wait 2 minutes for the measurements in the dashboard to be visible
			Debugger.Break();

            //Swap the two devices to ensure that the "ignore-broken"-implementation allows replacement of unreachable devices.
            //Swap device 1 and 3 (Simulates a replacement of device 3 with device 1).
            await new SwapPhysicalDevicesHandler(store, watson).Handle(new SwapPhysicalDevices
            {
                DeviceAId = device1.DeviceId,
                DeviceATypeId = device1.DeviceType,
                DeviceBId = device3.DeviceId,
                DeviceBTypeId = device3.DeviceType,
                UserId = userId,
                IgnoreNoAcknowledgementFromB = true
			});

			//Observe that the "broken"-device now seems to send data again (device 1 replaced device 3)
			//Wait 2 minutes for the measurements in the dashboard to be visible
			Debugger.Break();

            #endregion
        }
    }
}